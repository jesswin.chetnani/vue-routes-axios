import {
    createRouter,
    createWebHistory,
    createWebHashHistory
} from "vue-router";
import Home from "../Views/Home"
import About from "../Views/About"
import Jobs from "../Views/jobs/Jobs"
import JobDetails from "../Views/jobs/JobDetails"
import NotFound from "../Views/NotFound"
import AddDetails from "../Views/AddDetails"
import NestedComponent from "../Views/NestedComponent"
import NestedComponent2 from "../Views/NestedComponent2"
import Catelog from '../Views/Catelog'


const routes = [{
        path: "/",
        name: 'Home',
        component: Home
    }, {
        path: "/about",
        name: 'About',
        component: About
    }, {
        path: "/jobs",
        name: 'Jobs',
        component: Jobs
    },
    {
        path: "/jobs/:id",
        name: 'JobDetials',
        component: JobDetails,
        children: [{
            path: "nested",
            name: 'NestedComponent',
            component: NestedComponent
        }, {
            path: "nested2/:name",
            name: 'Nested2',
            component: NestedComponent2,
            props: true
        }]
    },
    {
        path: "/add-details",
        name: 'AddDetails',
        component: AddDetails
    },
    {
        path: "/catelog",
        name: 'Catelog',
        component: Catelog
    },
    //redirect
    {
        path: "/all-jobs",
        redirect: "/jobs"
    },

    //catch 404
    {
        path: "/:catchAll(.*)",
        name: 'NotFound',
        component: NotFound
    }
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    // history: createWebHashHistory(),
    routes
})

export default router